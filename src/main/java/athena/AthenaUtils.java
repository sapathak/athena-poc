package athena;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.Properties;

public class AthenaUtils {
    private static final Logger logger = LoggerFactory.getLogger(AthenaUtils.class);

    public static void setCredentials() throws Exception{
        Properties properties = new Properties();
        properties.load(new FileReader(AthenaConstants.DEFAULT_TOKEN_PROP_FILE));
        System.setProperty("aws.accessKeyId", properties.getProperty("AWS_ACCESS_KEY_ID"));
        System.setProperty("aws.secretAccessKey", properties.getProperty("AWS_SECRET_ACCESS_KEY"));
        System.setProperty("aws.sessionToken", properties.getProperty("AWS_SESSION_TOKEN"));
    }

    public static String getSKUs(){
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String line = null;
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("sku.txt")))){
            line = bufferedReader.readLine();
        }
        catch (FileNotFoundException ex) {
            logger.error("SKUs file not found", ex);
        } catch (IOException ex) {
            logger.error("Unable to extract SKUs from file", ex);
        }

        return line;
    }

    public static void writeQueryTimingsToFile(List<String> queryTimings, String fileName) throws Exception{
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(fileName), true))) {
            queryTimings.stream().forEach(queryTiming -> {
                try {
                    bufferedWriter.append(queryTiming + "\n");
                } catch (IOException e) {
                    logger.error("Unable to write timing to file. Timing: " + queryTiming, e);
                }
            });
        } catch (IOException e) {
            logger.error("Unable to write query timings to file.", e);
        }
    }
}
