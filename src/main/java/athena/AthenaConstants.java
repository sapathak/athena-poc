package athena;

public class AthenaConstants {
    public static final int SLEEP_AMOUNT_IN_MS = 1000;
    public static final String ATHENA_DATABASE = "cht_svc_pricing";
    public static final String ATHENA_OUTPUT_S3_FOLDER_PATH = "s3://titans-blr-experiments/athena-java-results/";

    public static final String COUNT_PRICES_QUERY = "SELECT COUNT(1) FROM PUBLIC_PRICES";
    public static final String SELECT_PRICES_QUERY = "SELECT RateCode  AS id, PricePerUnit as  discountedRate, PricePerUnit as publicRate,  Unit AS unit,  PriceDescription as description, SKU  as sku, ServiceCode as  productCode, ProductFamily as productFamily,  InstanceType as\n" +
            "instanceType, InstanceFamily as instanceFamily, DERIVED_REGION as locationName, DERIVED_PURCHASE_OPTION as  purchaseOption, usageType as usageType, operation as  operation, DERIVED_LEASE_CONTRACT_LEN as leaseContractLength,\n" +
            "OfferingClass as offeringClass, OperatingSystem as operatingSystem, Tenancy as tenancy, LicenseModel  as licenseModel, PreInstalledSW as preInstalledSoftware, CapacityStatus as capacityStatus, DERIVED_NAME as chDerivedOsName\n" +
            "FROM %s WHERE derived_purchase_option  = 'ALL_UPFRONT' AND SKU IN (" + AthenaUtils.getSKUs() + ")";

    public static final String DEFAULT_TOKEN_PROP_FILE = System.getProperty("user.home") + "/temp/token.properties";
}
