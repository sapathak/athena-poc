package athena;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.athena.AthenaClient;
import software.amazon.awssdk.services.athena.model.*;
import software.amazon.awssdk.services.athena.paginators.GetQueryResultsIterable;

import java.util.ArrayList;
import java.util.List;


public class AthenaMain {
    private static final Logger logger = LoggerFactory.getLogger(AthenaMain.class);


    public static void main(String[] args) throws Exception{
        AthenaUtils.setCredentials();

        AthenaClientFactory factory = new AthenaClientFactory();
        AthenaClient athenaClient = factory.createClient();

        int count = 1;
        List<String> queryTimings = new ArrayList<>();

        for(int i = 1; i<=count; i++){
            String queryExecutionId = submitAthenaQuery(athenaClient, String.format(AthenaConstants.SELECT_PRICES_QUERY, "PUBLIC_PRICES_PARTITIONED"));

            waitForQueryToComplete(athenaClient, queryExecutionId);

            processQueryExecutionTime(athenaClient, queryExecutionId, queryTimings);

            Thread.sleep(10000);
        }

        AthenaUtils.writeQueryTimingsToFile(queryTimings, "/Users/sapathak/Documents/sample.txt");

        //processResultRows(athenaClient, queryExecutionId);
    }

    private static String submitAthenaQuery(AthenaClient athenaClient, String query) {

        QueryExecutionContext queryExecutionContext = QueryExecutionContext.builder()
                .database(AthenaConstants.ATHENA_DATABASE).build();

        ResultConfiguration resultConfiguration = ResultConfiguration.builder()
                .outputLocation(AthenaConstants.ATHENA_OUTPUT_S3_FOLDER_PATH).build();

        StartQueryExecutionRequest startQueryExecutionRequest = StartQueryExecutionRequest.builder()
                .queryString(query)
                .queryExecutionContext(queryExecutionContext)
                .resultConfiguration(resultConfiguration).build();

        StartQueryExecutionResponse startQueryExecutionResponse = athenaClient.startQueryExecution(startQueryExecutionRequest);

        return startQueryExecutionResponse.queryExecutionId();
    }

    private static void waitForQueryToComplete(AthenaClient athenaClient, String queryExecutionId) throws InterruptedException {

        GetQueryExecutionRequest getQueryExecutionRequest = GetQueryExecutionRequest.builder()
                .queryExecutionId(queryExecutionId).build();

        GetQueryExecutionResponse queryExecutionResponse;

        boolean isQueryStillRunning = true;

        while (isQueryStillRunning) {
            queryExecutionResponse = athenaClient.getQueryExecution(getQueryExecutionRequest);
            String queryState = queryExecutionResponse.queryExecution().status().state().toString();

            if (queryState.equals(QueryExecutionState.FAILED.toString())) {
                throw new RuntimeException("Query Failed to run with Error Message: " + queryExecutionResponse
                        .queryExecution().status().stateChangeReason());
            } else if (queryState.equals(QueryExecutionState.CANCELLED.toString())) {
                throw new RuntimeException("Query was cancelled.");
            } else if (queryState.equals(QueryExecutionState.SUCCEEDED.toString())) {
                isQueryStillRunning = false;
            } else {
                Thread.sleep(AthenaConstants.SLEEP_AMOUNT_IN_MS);
            }
        }
    }

    private static void processQueryExecutionTime(AthenaClient athenaClient, String queryExecutionId, List<String> queryTimings){
        GetQueryExecutionRequest getQueryExecutionRequest = GetQueryExecutionRequest.builder()
                .queryExecutionId(queryExecutionId).build();

        QueryExecutionStatistics queryExecutionStatistics =  athenaClient.getQueryExecution(getQueryExecutionRequest).queryExecution().statistics();

        if(queryExecutionStatistics != null){
            String queryTiming = queryExecutionStatistics.engineExecutionTimeInMillis()/1000.0 + " seconds";
            queryTimings.add(queryTiming);
            logger.info("Time taken to execute query = " + queryTiming);
        }
    }

    private static void processResultRows(AthenaClient athenaClient, String queryExecutionId) {

        GetQueryResultsRequest getQueryResultsRequest = GetQueryResultsRequest.builder()
                .queryExecutionId(queryExecutionId).build();

        GetQueryResultsIterable getQueryResultsResults = athenaClient.getQueryResultsPaginator(getQueryResultsRequest);

        for (GetQueryResultsResponse result : getQueryResultsResults) {
            List<ColumnInfo> columnInfoList = result.resultSet().resultSetMetadata().columnInfo();

            int resultSize = result.resultSet().rows().size();
            logger.info("Result size: " + resultSize);

            List<Row> results = result.resultSet().rows();
            processRow(results, columnInfoList);
        }
    }

    private static void processRow(List<Row> rowList, List<ColumnInfo> columnInfoList) {

        List<String> columns = new ArrayList<>();

        for (ColumnInfo columnInfo : columnInfoList) {
            columns.add(columnInfo.name());
        }

        for (Row row: rowList) {
            int index = 0;

            for (Datum datum : row.data()) {
                logger.info(columns.get(index) + ": " + datum.varCharValue());
                index++;
            }

            logger.info("===================================");
        }
    }
}
