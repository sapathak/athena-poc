package athena;

import software.amazon.awssdk.auth.credentials.SystemPropertyCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.athena.AthenaClient;
import software.amazon.awssdk.services.athena.AthenaClientBuilder;

class AthenaClientFactory {

    AthenaClient createClient() {
        final AthenaClientBuilder builder = AthenaClient.builder()
                .region(Region.US_EAST_1)
                .credentialsProvider(SystemPropertyCredentialsProvider.create());

        return builder.build();
    }
}
